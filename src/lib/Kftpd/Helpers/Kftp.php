<?php

namespace Kftpd\Helpers;

class Kftp{

    public $conn;

    public function __construct($url){
        $this->conn = ftp_connect($url);
    }

    public function getConn()
    {
        return $this->conn;
    }

    // recursive make directory function for ftp
    public function make_directory($dir)
    {
        // if directory already exists or can be immediately created return true
        if ($this->is_dir($dir) || @$this->ftp_mkdir($dir)) return true;
        // otherwise recursively try to make the directory

        if (!$this->make_directory(dirname($dir))) return false;
        // final step to create the directory
        return $this->ftp_mkdir($dir);
    }

    public function is_dir($dir)
    {
        // get current directory
        $original_directory = $this->ftp_pwd();
        // test if you can change directory to $dir
        // suppress errors in case $dir is not a file or not a directory
        if ( @$this->ftp_chdir($dir ) ) {
        // If it is a directory, then change the directory back to the original directory
            $this->ftp_chdir($original_directory );
            return true;
        } else {
            return false;
        }
    }

    public function __call($func,$a){

        if(strstr($func,'ftp_') == false)
        {
            return call_user_func_array($this->$fund,$a);
        }

        if(strstr($func,'ftp_') !== false && function_exists($func)){
            array_unshift($a,$this->conn);
            return call_user_func_array($func,$a);
        }else{
            // replace with your own error handler.
            die("$func is not a valid FTP function");
        }
    }

}