<?php

namespace Kftpd\Helpers;

class Custom {

	public static function is_dir_empty($dir) {
	  if (!is_readable($dir)) return NULL;
	  $handle = opendir($dir);
	  while (false !== ($entry = readdir($handle))) {
	    if ($entry != "." && $entry != "..") {
	      return FALSE;
	    }
	  }
	  return TRUE;
	}

	/**
	 * [pathExtract]
	 * @param  [string] $file [description]
	 * @return [array]       [description]
	 */
	public static function pathExtract($file)
	{
		$filePath = explode('/', $file);
		$fileName = array_pop($filePath);
		$retorno = array('path' => implode('/', $filePath), 'name' => $fileName);
		return $retorno;
	}
}