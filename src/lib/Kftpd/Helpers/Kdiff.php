<?php

namespace Kftpd\Helpers;

class Kdiff {

	public static function fileList()
	{
		/**
		 * [$lastCommit description]
		 * @var [type]
		 */
		$lastCommit = trim(shell_exec("git rev-parse --short HEAD"));

		if (Custom::is_dir_empty(KREVISION))
		{
			$startCommit = trim(shell_exec("git rev-list --abbrev-commit HEAD | tail -n 1"));
		}else{
			//getLast File
			$lastRevision = scandir(KREVISION, 1);
			$lastRevision = $lastRevision[0];
			$revision = file_get_contents(KREVISION.$lastRevision);

			$revLast = explode('..',$revision);
			$startCommit = trim($revLast[1]);
		}

		$fileList = shell_exec("git diff --diff-filter=ACMRTUXB --name-only ".$startCommit." ".$lastCommit);

		$filesToApply = preg_split('/$\R?^/m', trim($fileList));

		return array(
			'startCommit' => $startCommit,
			'lastCommit' => $lastCommit,
			'files' => $filesToApply
		);
	}

}//