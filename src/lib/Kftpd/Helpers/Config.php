<?php

namespace Kftpd\Helpers;

class Config {

	private static $confKeys;

	public static function get($key)
	{
		if (array_key_exists($key,self::$confKeys))
			return self::$confKeys[$key];

		return null;
	}


	public static function load()
	{
		self::$confKeys = include KFTPD_PATH.DS.'kftphistory'.DS.'config.php';
	}

}