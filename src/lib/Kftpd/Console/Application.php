<?php namespace Kftpd\Console;

use ErrorException;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Kftpd\Commands as Command;
use Kftpd\Helpers;

class Application extends \Symfony\Component\Console\Application {

    /**
     * The exception handler instance.
     *
     * @var \Illuminate\Exception\Handler
     */
    protected $exceptionHandler;

    /**
     * @override
     */
    public function __construct($name = 'Kftpd Console', $version = '0.9')
    {
        // convert errors to exceptions
        set_error_handler(
            function ($code, $message, $file, $line) {
                if (error_reporting() & $code) {
                    throw new ErrorException($message, 0, $code, $file, $line);
                }
            }
        );

        define('KREVISION',KFTPD_PATH.DS.'kftphistory'.DS.'revisions'.DS);

        if (is_dir(KREVISION))
            Helpers\Config::load();

        parent::__construct($name, $version);
    }

    /**
     * @override
     */
    protected function getDefaultCommands()
    {
        $commands = parent::getDefaultCommands();

        //if (extension_loaded('phar')) {}

        $commands[] = new Command\Setup();
        $commands[] = new Command\Verify();
        $commands[] = new Command\Doit();
        $commands[] = new Command\About();

        return $commands;
    }


    /**
     * Start a new Console application.
     *
     * @return \Kftpd\Console\Application
     */
    public static function start()
    {
        $app = new self();

        return $app->run();
    }

    /**
     * Add a command to the console.
     *
     * @param  \Symfony\Component\Console\Command\Command  $command
     * @return \Symfony\Component\Console\Command\Command
     */
    public function add(SymfonyCommand $command)
    {
        if ($command instanceof Command)
        {
            //$command->setLaravel($this->laravel);
        }

        return $this->addToParent($command);
    }

    /**
     * Add the command to the parent instance.
     *
     * @param  \Symfony\Component\Console\Command\Command  $command
     * @return \Symfony\Component\Console\Command\Command
     */
    protected function addToParent(SymfonyCommand $command)
    {
        return parent::add($command);
    }

    /**
     * Add a command, resolving through the application.
     *
     * @param  string  $command
     * @return \Symfony\Component\Console\Command\Command
     */
    public function resolve($command)
    {
        return $this->add($command);
    }

    /**
     * Resolve an array of commands through the application.
     *
     * @param  array|dynamic  $commands
     * @return void
     */
    public function resolveCommands($commands)
    {
        $commands = is_array($commands) ? $commands : func_get_args();

        foreach ($commands as $command)
        {
            $this->resolve($command);
        }
    }

    /**
     * Get the default input definitions for the applications.
     *
     * @return \Symfony\Component\Console\Input\InputDefinition
     */
    protected function getDefaultInputDefinition()
    {
        $definition = parent::getDefaultInputDefinition();

        $definition->addOption($this->getEnvironmentOption());

        return $definition;
    }

    /**
     * Get the global environment option for the definition.
     *
     * @return \Symfony\Component\Console\Input\InputOption
     */
    protected function getEnvironmentOption()
    {
        $message = 'The environment the command should run under.';

        return new InputOption('--env', null, InputOption::VALUE_OPTIONAL, $message);
    }

    /**
     * Render the given exception.
     *
     * @param  \Exception  $e
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function renderException($e, $output)
    {
        // If we have an exception handler instance, we will call that first in case
        // it has some handlers that need to be run first. We will pass "true" as
        // the second parameter to indicate that it's handling a console error.
        if (isset($this->exceptionHandler))
        {
            $this->exceptionHandler->handleConsole($e);
        }

        parent::renderException($e, $output);
    }

    /**
     * Set the exception handler instance.
     *
     * @param  \Illuminate\Exception\Handler  $handler
     * @return void
     */
    public function setExceptionHandler($handler)
    {
        $this->exceptionHandler = $handler;
    }

}
//