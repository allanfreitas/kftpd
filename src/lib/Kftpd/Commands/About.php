<?php

namespace Kftpd\Commands;

/**
 * @author    Allan Freitas <allanfreitasci@gmail.com>
 * About
 */
use Kftpd\Helpers;
use Kftpd\Console\BaseCommand as BaseCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

//
class About extends BaseCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'about';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "About Kftpd";

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('force', null, InputOption::VALUE_NONE, 'Force the compiled class file to be written.'),
        );
    }

    protected function getArguments()
    {
        return array(
            //array
        );
    }

    public function fire()
    {
        //$this->info('Kftpd Deploy');
        $this->info('');
        $this->info('Version: 0.9');
        $this->info('');
        $this->info('Update you app or website using GIT + FTP');
    }

}//