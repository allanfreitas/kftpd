<?php

namespace Kftpd\Commands;

/**
 * @author    Allan Freitas <allanfreitasci@gmail.com>
 * Setup
 */

use Kftpd\Console\BaseCommand as BaseCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

//
class Setup extends BaseCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'setup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Setup Kftpd Deploy";

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('force', null, InputOption::VALUE_NONE, 'Force the compiled class file to be written.'),
        );
    }

    protected function getArguments()
    {
        return array(
            //array
        );
    }


    public function fire()
    {
      $base_path = KFTPD_PATH.DIRECTORY_SEPARATOR.'kftphistory';

      $this->info('BasePath from PHAR:');
      $this->info($base_path);

      if (!is_dir($base_path)){

        if ($this->confirm('Kftpd Folder not found, Can I create?')){

          $this->info('- Create Folder: kftphistory');
          if (mkdir($base_path)){
            $this->info('- Create Folder: kftphistory/revisions');
            mkdir($base_path.DIRECTORY_SEPARATOR.'revisions');

            if (file_put_contents($base_path.DIRECTORY_SEPARATOR.'config.php',
              $this->configFile()) <> false)
            {
              $this->info('- Create File: kftphistory/config.php');
            }

            
          }

          $this->info('Done. Kftpd Setup Complete!');

        }else{
          $this->info('Abort. Kftpd Setup Aborted!');
        }
        
        

      }

    }

    protected function configFile()
    {
      $fileContent = <<<EOF
<?php
/**
 * Kftpd Deploy
 * Configuration File
 */

return array(
  'ftpHost' => '', //FTP IP or Host
  'ftpUser' => '', //FTP Username
  'ftpPass' => '', //FTP Password
  'ftpPublic' => '/public_html/', //FTP Public Path (CPANEL accounts are /public_html/)
  //You Can Add ExcludedFiles in this list
  'excludedFiles' => array(
    '.gitkeep',
    '.gitattributes',
    '.gitignore',
  ),
);
EOF;

      return $fileContent;
    }

}