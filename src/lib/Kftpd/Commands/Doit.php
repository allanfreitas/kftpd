<?php

namespace Kftpd\Commands;

/**
 * @author    Allan Freitas <allanfreitasci@gmail.com>
 * Doit
 */
use Kftpd\Helpers;
use Kftpd\Console\BaseCommand as BaseCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

//
class Doit extends BaseCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'doit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Kftpd Apply Updates";

    protected $excludedFiles;
    protected $publicPath;
    protected $kftp;

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('force', null, InputOption::VALUE_NONE, 'Force the compiled class file to be written.'),
        );
    }

    protected function getArguments()
    {
        return array(
            //array
        );
    }

    public function fire()
    {
        $filesDiff = Helpers\Kdiff::fileList();

        $this->info('-----------------------------------');
        $this->info('-------------- Diff -----------------');
        $this->info('From: '.$filesDiff['startCommit']);
        $this->info('To  : '.$filesDiff['lastCommit']);
        $this->info('------------ File List --------------');

        $this->excludedFiles = Helpers\Config::get('excludedFiles');
        $this->publicPath = Helpers\Config::get('ftpPublic');

        /**
         * Open FTP Connection
         */
        $this->kftpInit();

        $i = 0;
        foreach ($filesDiff['files'] as $v)
        {
            $path_name = Helpers\Custom::pathExtract($v);

            if (!in_array($path_name['name'],$this->excludedFiles))
            {
                $this->filePut($v,$path_name);
                $i++;
            }
        }

        $this->info('-----------------------------------');
        if ($i == 0)
        {
            $this->info('No files Applyed');
        }else{
            $this->info('Total Files Applyed: '.$i);
        }
        $this->info('-----------------------------------');

        /**
         * Grava Revision
         */
        $newDeploy = $filesDiff['startCommit'].'..'.$filesDiff['lastCommit'];
        file_put_contents(KREVISION.date("YmdHis").'.txt',$newDeploy);
    }

    protected function kftpInit()
    {
        $this->kftp = new Helpers\Kftp(Helpers\Config::get('ftpHost'));
        $this->kftp->ftp_login(
            Helpers\Config::get('ftpUser'),
            Helpers\Config::get('ftpPass')
        );
        $this->kftp->ftp_pasv(true);
    }

    protected function filePut($v,$path_name)
    {

        if (!in_array($path_name['name'],$this->excludedFiles)
            AND
            (!in_array($path_name['path'],$this->excludedFiles))
            )
        {
            $filePath = $this->publicPath.$path_name['path'];

            /**
             *
             */
            if (is_dir($path_name['path']) OR $path_name['path'] !== '')
            {
                /**
                 * If directory exists write file
                 */
                if ($this->kftp->is_dir($filePath))
                {
                    $this->writeFile($v);
                /**
                 * "else" directory NOT exists try to create the directory and write file
                 */
                }else{
                    /**
                     * try to create the directory
                     */
                    if ($this->kftp->make_directory($filePath))
                    {
                        $this->info('Diretorio criado: '.$path_name['path'],false);
                        $this->writeFile($v);
                    /**
                     * Error on create the directory (keep going)
                     */
                    }else{
                        $this->info('Erro ao Criar o Diretorio: '.$path_name['path'],false);
                    }
                }
            }else{
                $this->writeFile($v);
            }
        }//if excludedFiles

    }

    protected function writeFile($v)
    {
        //if ($this->kftp->ftp_put($this->publicPath.$v, $v, FTP_ASCII))
        if ($this->kftp->ftp_put($this->publicPath.$v, $v, FTP_BINARY))
        {
            $this->info('Arquivo Enviado: '.$this->publicPath.$v,false);
        }else{
            $this->info('Erro ao Enviar o Arquivo: '.$this->publicPath.$v,false);
        }
    }

}//