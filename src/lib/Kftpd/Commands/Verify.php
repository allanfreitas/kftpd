<?php

namespace Kftpd\Commands;

/**
 * @author    Allan Freitas <allanfreitasci@gmail.com>
 * Verify
 */
use Kftpd\Helpers;
use Kftpd\Console\BaseCommand as BaseCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

//
class Verify extends BaseCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'verify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Kftpd Verify Updates";

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('force', null, InputOption::VALUE_NONE, 'Force the compiled class file to be written.'),
        );
    }

    protected function getArguments()
    {
        return array(
            //array
        );
    }

    public function fire()
    {
        $filesDiff = Helpers\Kdiff::fileList();
		//$filesToApply = Helpers\Kdiff::fileList();
		//$fileCount = count($filesDiff['files']);

        $this->info('-----------------------------------');
        $this->info('-------------- Diff -----------------');
        $this->info('From: '.$filesDiff['startCommit']);
        $this->info('To  : '.$filesDiff['lastCommit']);
        $this->info('------------ File List --------------');

        $excludedFiles = Helpers\Config::get('excludedFiles');

        $i = 0;
        foreach ($filesDiff['files'] as $v)
        {
            $path_name = Helpers\Custom::pathExtract($v);

            if (!in_array($path_name['name'],$excludedFiles))
            {
			    $this->info($v);
                $i++;
            }
		}

        $this->info('-----------------------------------');
        if ($i == 0)
        {
            $this->info('No files to Apply');
        }else{
            $this->info('Total Files To Apply: '.$i);
        }
		$this->info('-----------------------------------');
    }

}//