#!/bin/sh
find src/vendor/ -type d -name "Tests" -exec rm -rf {} \;
find src/vendor/ -type d -name "Tester" -exec rm -rf {} \;
find src/vendor/ -type f -name "CHANGELOG*" -exec rm -rf {} \;
find src/vendor/ -type f -name "README*" -exec rm -rf {} \;
find src/vendor/ -type f -name "LICENSE*" -exec rm -rf {} \;
find src/vendor/ -type f -name "phpunit.xml*" -exec rm -rf {} \;